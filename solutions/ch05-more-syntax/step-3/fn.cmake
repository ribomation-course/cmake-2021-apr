function(numbers)
    message("--- numbers ---")
    foreach(k RANGE 1 5)
        message("${k}")
    endforeach()
endfunction()

function(words)
    message("--- words ---")
    foreach(k IN ITEMS foo bar fee hepp)
        message("${k}")
    endforeach()
endfunction()

