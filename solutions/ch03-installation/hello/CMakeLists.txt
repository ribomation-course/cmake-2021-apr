cmake_minimum_required(VERSION 3.12)
project(hello LANGUAGES CXX)

add_executable(hello hello.cxx)
