#include <iostream>
#include <string>
#include <boost/circular_buffer.hpp>
using namespace std;
using namespace std::literals;

template <typename T>
using CircularBuffer = boost::circular_buffer<T>;

void populate(CircularBuffer<int>& buf) {
  static auto nextValue = 1;
  auto N = buf.capacity();
  while (N-- > 0)
    buf.push_back(nextValue++);
}

void print(const CircularBuffer<int>& buf) {
  cout << "buf.size = " << buf.size() << '\n';
  for (auto k : buf)
    cout << "- item: " << k << '\n';
}

int main(int argc, char** argv) {
  const auto numTurns = (argc <= 1) ? 7 : stoi(argv[1]);
  cout << "# turns : " << numTurns << endl;

  auto  buf           = CircularBuffer<int>{3};
  cout << "capacity: " << buf.capacity() << '\n';
  cout << "size    : " << buf.size() << '\n';

  for (auto k = 0; k < numTurns; ++k) {
    populate(buf);
    print(buf);
  }

  return 0;
}
