# Build & Run

    mkdir bld && cd bld
    cmake -G Ninja ..
    cmake --build . -- -v
    cmake --build . --target run

