#define CATCH_CONFIG_MAIN   //once per executable
#include "catch.hpp"
#include "numbers.hxx"

SCENARIO("testing fibonacci()", "[base]") {
    GIVEN("a lousy function") {
        WHEN("the arg is 10") {
            THEN("it should return 55") {
                REQUIRE(fibonacci(10) == 55);
            }
        }
		
		WHEN("the arg is 42") {
            THEN("it should return 267914296") {
                REQUIRE(fibonacci(42) == 267914296);
            }
        }
    }
}


SCENARIO("testing sum()", "[base]") {
    GIVEN("another lousy function") {
        WHEN("the arg is 10") {
            THEN("it should return 55") {
                REQUIRE(sum(10) == 55);
            }
        }
		
		WHEN("the arg is 1") {
            THEN("it should return 1") {
                REQUIRE(sum(1) == 1);
            }
        }
    }
}


