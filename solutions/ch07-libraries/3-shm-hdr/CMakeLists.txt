cmake_minimum_required(VERSION 3.12)
project(shm-app 
	VERSION		1
  #  LANGUAGES	CXX
)

enable_language(CXX)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)
set(CMAKE_BUILD_TYPE 			Release)


add_library(shm INTERFACE)
target_sources(shm
	INTERFACE ${CMAKE_SOURCE_DIR}/src/hdrs/shared-memory-hdroly.hxx
)
target_include_directories(shm
	INTERFACE ${CMAKE_SOURCE_DIR}/src/hdrs
)
target_compile_options(shm
	INTERFACE -Wall -Wextra -Wfatal-errors -Wno-pointer-arith
)
target_link_libraries(shm
	INTERFACE  rt
)


add_executable(shm-app src/cxx/app.cxx )
target_compile_definitions(shm-app PRIVATE NDEBUG=1 )
target_link_libraries(shm-app  shm )


add_custom_target(run
	COMMAND $<TARGET_FILE:shm-app> 40
)

# mkdir bld && cd bld && cmake .. && cmake --build . && cmake --build . --target run
