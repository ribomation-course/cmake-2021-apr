#include <iostream>
#include <string>
#include "account.hxx"
#include "util.hxx"
using namespace std;
using namespace std::literals;

int main() {
	Account acc{150};
	cout << "acc: " << acc.getBalance() << endl;
	acc.update(250);
	cout << "acc: " << acc.getBalance() << endl;
	
	string txt = "tjolla hopp"s;
	cout << txt << " --> " << toUpperCase(txt) << endl;
	return 0;
}
