#include <sstream>
#include "yaml-cpp/yaml.h"
#include "yaml-codec.hxx"
using namespace std;

string YamlCodec::toYaml(vector<string> list) {
    ostringstream buf;
	YAML::Emitter yaml{buf};

    yaml << YAML::BeginSeq;
    for (auto w : list) 
        yaml << w;
    yaml << YAML::EndSeq;

    return buf.str();
}

string YamlCodec::toYaml(map<string, string> object) {
    ostringstream buf;
	YAML::Emitter yaml{buf};

    yaml << YAML::BeginMap;
    for (auto [key,value] : object) 
        yaml << YAML::Key << key << YAML::Value << value;
    yaml << YAML::EndMap;

    return buf.str();
}


